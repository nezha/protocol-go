// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.23.0
// 	protoc        v3.13.0
// source: nezha/upload/rpc.proto

package upload

import (
	context "context"
	proto "github.com/golang/protobuf/proto"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

// This is a compile-time assertion that a sufficiently up-to-date version
// of the legacy proto package is being used.
const _ = proto.ProtoPackageIsVersion4

var File_nezha_upload_rpc_proto protoreflect.FileDescriptor

var file_nezha_upload_rpc_proto_rawDesc = []byte{
	0x0a, 0x16, 0x6e, 0x65, 0x7a, 0x68, 0x61, 0x2f, 0x75, 0x70, 0x6c, 0x6f, 0x61, 0x64, 0x2f, 0x72,
	0x70, 0x63, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x0c, 0x6e, 0x65, 0x7a, 0x68, 0x61, 0x2e,
	0x75, 0x70, 0x6c, 0x6f, 0x61, 0x64, 0x1a, 0x16, 0x6e, 0x65, 0x7a, 0x68, 0x61, 0x2f, 0x75, 0x70,
	0x6c, 0x6f, 0x61, 0x64, 0x2f, 0x61, 0x64, 0x64, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x1a, 0x1b,
	0x6e, 0x65, 0x7a, 0x68, 0x61, 0x2f, 0x75, 0x70, 0x6c, 0x6f, 0x61, 0x64, 0x2f, 0x69, 0x6e, 0x69,
	0x74, 0x69, 0x61, 0x74, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x1a, 0x18, 0x6e, 0x65, 0x7a,
	0x68, 0x61, 0x2f, 0x75, 0x70, 0x6c, 0x6f, 0x61, 0x64, 0x2f, 0x61, 0x62, 0x6f, 0x72, 0x74, 0x2e,
	0x70, 0x72, 0x6f, 0x74, 0x6f, 0x1a, 0x1b, 0x6e, 0x65, 0x7a, 0x68, 0x61, 0x2f, 0x75, 0x70, 0x6c,
	0x6f, 0x61, 0x64, 0x2f, 0x63, 0x6f, 0x6d, 0x70, 0x6c, 0x65, 0x74, 0x65, 0x2e, 0x70, 0x72, 0x6f,
	0x74, 0x6f, 0x1a, 0x16, 0x6e, 0x65, 0x7a, 0x68, 0x61, 0x2f, 0x75, 0x70, 0x6c, 0x6f, 0x61, 0x64,
	0x2f, 0x67, 0x65, 0x74, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x32, 0xb2, 0x02, 0x0a, 0x03, 0x52,
	0x70, 0x63, 0x12, 0x33, 0x0a, 0x03, 0x41, 0x64, 0x64, 0x12, 0x14, 0x2e, 0x6e, 0x65, 0x7a, 0x68,
	0x61, 0x2e, 0x75, 0x70, 0x6c, 0x6f, 0x61, 0x64, 0x2e, 0x41, 0x64, 0x64, 0x52, 0x65, 0x71, 0x1a,
	0x14, 0x2e, 0x6e, 0x65, 0x7a, 0x68, 0x61, 0x2e, 0x75, 0x70, 0x6c, 0x6f, 0x61, 0x64, 0x2e, 0x41,
	0x64, 0x64, 0x52, 0x73, 0x70, 0x22, 0x00, 0x12, 0x33, 0x0a, 0x03, 0x47, 0x65, 0x74, 0x12, 0x14,
	0x2e, 0x6e, 0x65, 0x7a, 0x68, 0x61, 0x2e, 0x75, 0x70, 0x6c, 0x6f, 0x61, 0x64, 0x2e, 0x47, 0x65,
	0x74, 0x52, 0x65, 0x71, 0x1a, 0x14, 0x2e, 0x6e, 0x65, 0x7a, 0x68, 0x61, 0x2e, 0x75, 0x70, 0x6c,
	0x6f, 0x61, 0x64, 0x2e, 0x47, 0x65, 0x74, 0x52, 0x73, 0x70, 0x22, 0x00, 0x12, 0x42, 0x0a, 0x08,
	0x49, 0x6e, 0x69, 0x74, 0x69, 0x61, 0x74, 0x65, 0x12, 0x19, 0x2e, 0x6e, 0x65, 0x7a, 0x68, 0x61,
	0x2e, 0x75, 0x70, 0x6c, 0x6f, 0x61, 0x64, 0x2e, 0x49, 0x6e, 0x69, 0x74, 0x69, 0x61, 0x74, 0x65,
	0x52, 0x65, 0x71, 0x1a, 0x19, 0x2e, 0x6e, 0x65, 0x7a, 0x68, 0x61, 0x2e, 0x75, 0x70, 0x6c, 0x6f,
	0x61, 0x64, 0x2e, 0x49, 0x6e, 0x69, 0x74, 0x69, 0x61, 0x74, 0x65, 0x52, 0x73, 0x70, 0x22, 0x00,
	0x12, 0x39, 0x0a, 0x05, 0x41, 0x62, 0x6f, 0x72, 0x74, 0x12, 0x16, 0x2e, 0x6e, 0x65, 0x7a, 0x68,
	0x61, 0x2e, 0x75, 0x70, 0x6c, 0x6f, 0x61, 0x64, 0x2e, 0x41, 0x62, 0x6f, 0x72, 0x74, 0x52, 0x65,
	0x71, 0x1a, 0x16, 0x2e, 0x6e, 0x65, 0x7a, 0x68, 0x61, 0x2e, 0x75, 0x70, 0x6c, 0x6f, 0x61, 0x64,
	0x2e, 0x41, 0x62, 0x6f, 0x72, 0x74, 0x52, 0x73, 0x70, 0x22, 0x00, 0x12, 0x42, 0x0a, 0x08, 0x43,
	0x6f, 0x6d, 0x70, 0x6c, 0x65, 0x74, 0x65, 0x12, 0x19, 0x2e, 0x6e, 0x65, 0x7a, 0x68, 0x61, 0x2e,
	0x75, 0x70, 0x6c, 0x6f, 0x61, 0x64, 0x2e, 0x43, 0x6f, 0x6d, 0x70, 0x6c, 0x65, 0x74, 0x65, 0x52,
	0x65, 0x71, 0x1a, 0x19, 0x2e, 0x6e, 0x65, 0x7a, 0x68, 0x61, 0x2e, 0x75, 0x70, 0x6c, 0x6f, 0x61,
	0x64, 0x2e, 0x43, 0x6f, 0x6d, 0x70, 0x6c, 0x65, 0x74, 0x65, 0x52, 0x73, 0x70, 0x22, 0x00, 0x42,
	0x3f, 0x0a, 0x10, 0x63, 0x6f, 0x6d, 0x2e, 0x6e, 0x65, 0x7a, 0x68, 0x61, 0x2e, 0x75, 0x70, 0x6c,
	0x6f, 0x61, 0x64, 0x50, 0x01, 0x5a, 0x29, 0x67, 0x69, 0x74, 0x65, 0x61, 0x2e, 0x63, 0x6f, 0x6d,
	0x2f, 0x6e, 0x65, 0x7a, 0x68, 0x61, 0x2f, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x63, 0x6f, 0x6c, 0x2d,
	0x67, 0x6f, 0x2f, 0x75, 0x70, 0x6c, 0x6f, 0x61, 0x64, 0x3b, 0x75, 0x70, 0x6c, 0x6f, 0x61, 0x64,
	0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var file_nezha_upload_rpc_proto_goTypes = []interface{}{
	(*AddReq)(nil),      // 0: nezha.upload.AddReq
	(*GetReq)(nil),      // 1: nezha.upload.GetReq
	(*InitiateReq)(nil), // 2: nezha.upload.InitiateReq
	(*AbortReq)(nil),    // 3: nezha.upload.AbortReq
	(*CompleteReq)(nil), // 4: nezha.upload.CompleteReq
	(*AddRsp)(nil),      // 5: nezha.upload.AddRsp
	(*GetRsp)(nil),      // 6: nezha.upload.GetRsp
	(*InitiateRsp)(nil), // 7: nezha.upload.InitiateRsp
	(*AbortRsp)(nil),    // 8: nezha.upload.AbortRsp
	(*CompleteRsp)(nil), // 9: nezha.upload.CompleteRsp
}
var file_nezha_upload_rpc_proto_depIdxs = []int32{
	0, // 0: nezha.upload.Rpc.Add:input_type -> nezha.upload.AddReq
	1, // 1: nezha.upload.Rpc.Get:input_type -> nezha.upload.GetReq
	2, // 2: nezha.upload.Rpc.Initiate:input_type -> nezha.upload.InitiateReq
	3, // 3: nezha.upload.Rpc.Abort:input_type -> nezha.upload.AbortReq
	4, // 4: nezha.upload.Rpc.Complete:input_type -> nezha.upload.CompleteReq
	5, // 5: nezha.upload.Rpc.Add:output_type -> nezha.upload.AddRsp
	6, // 6: nezha.upload.Rpc.Get:output_type -> nezha.upload.GetRsp
	7, // 7: nezha.upload.Rpc.Initiate:output_type -> nezha.upload.InitiateRsp
	8, // 8: nezha.upload.Rpc.Abort:output_type -> nezha.upload.AbortRsp
	9, // 9: nezha.upload.Rpc.Complete:output_type -> nezha.upload.CompleteRsp
	5, // [5:10] is the sub-list for method output_type
	0, // [0:5] is the sub-list for method input_type
	0, // [0:0] is the sub-list for extension type_name
	0, // [0:0] is the sub-list for extension extendee
	0, // [0:0] is the sub-list for field type_name
}

func init() { file_nezha_upload_rpc_proto_init() }
func file_nezha_upload_rpc_proto_init() {
	if File_nezha_upload_rpc_proto != nil {
		return
	}
	file_nezha_upload_add_proto_init()
	file_nezha_upload_initiate_proto_init()
	file_nezha_upload_abort_proto_init()
	file_nezha_upload_complete_proto_init()
	file_nezha_upload_get_proto_init()
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_nezha_upload_rpc_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   0,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_nezha_upload_rpc_proto_goTypes,
		DependencyIndexes: file_nezha_upload_rpc_proto_depIdxs,
	}.Build()
	File_nezha_upload_rpc_proto = out.File
	file_nezha_upload_rpc_proto_rawDesc = nil
	file_nezha_upload_rpc_proto_goTypes = nil
	file_nezha_upload_rpc_proto_depIdxs = nil
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConnInterface

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion6

// RpcClient is the client API for Rpc service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type RpcClient interface {
	// 创建上传
	Add(ctx context.Context, in *AddReq, opts ...grpc.CallOption) (*AddRsp, error)
	// 获取上传
	Get(ctx context.Context, in *GetReq, opts ...grpc.CallOption) (*GetRsp, error)
	// 初始化分片上传
	Initiate(ctx context.Context, in *InitiateReq, opts ...grpc.CallOption) (*InitiateRsp, error)
	// 取消上传
	Abort(ctx context.Context, in *AbortReq, opts ...grpc.CallOption) (*AbortRsp, error)
	// 完成上传
	Complete(ctx context.Context, in *CompleteReq, opts ...grpc.CallOption) (*CompleteRsp, error)
}

type rpcClient struct {
	cc grpc.ClientConnInterface
}

func NewRpcClient(cc grpc.ClientConnInterface) RpcClient {
	return &rpcClient{cc}
}

func (c *rpcClient) Add(ctx context.Context, in *AddReq, opts ...grpc.CallOption) (*AddRsp, error) {
	out := new(AddRsp)
	err := c.cc.Invoke(ctx, "/nezha.upload.Rpc/Add", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *rpcClient) Get(ctx context.Context, in *GetReq, opts ...grpc.CallOption) (*GetRsp, error) {
	out := new(GetRsp)
	err := c.cc.Invoke(ctx, "/nezha.upload.Rpc/Get", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *rpcClient) Initiate(ctx context.Context, in *InitiateReq, opts ...grpc.CallOption) (*InitiateRsp, error) {
	out := new(InitiateRsp)
	err := c.cc.Invoke(ctx, "/nezha.upload.Rpc/Initiate", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *rpcClient) Abort(ctx context.Context, in *AbortReq, opts ...grpc.CallOption) (*AbortRsp, error) {
	out := new(AbortRsp)
	err := c.cc.Invoke(ctx, "/nezha.upload.Rpc/Abort", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *rpcClient) Complete(ctx context.Context, in *CompleteReq, opts ...grpc.CallOption) (*CompleteRsp, error) {
	out := new(CompleteRsp)
	err := c.cc.Invoke(ctx, "/nezha.upload.Rpc/Complete", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// RpcServer is the server API for Rpc service.
type RpcServer interface {
	// 创建上传
	Add(context.Context, *AddReq) (*AddRsp, error)
	// 获取上传
	Get(context.Context, *GetReq) (*GetRsp, error)
	// 初始化分片上传
	Initiate(context.Context, *InitiateReq) (*InitiateRsp, error)
	// 取消上传
	Abort(context.Context, *AbortReq) (*AbortRsp, error)
	// 完成上传
	Complete(context.Context, *CompleteReq) (*CompleteRsp, error)
}

// UnimplementedRpcServer can be embedded to have forward compatible implementations.
type UnimplementedRpcServer struct {
}

func (*UnimplementedRpcServer) Add(context.Context, *AddReq) (*AddRsp, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Add not implemented")
}
func (*UnimplementedRpcServer) Get(context.Context, *GetReq) (*GetRsp, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Get not implemented")
}
func (*UnimplementedRpcServer) Initiate(context.Context, *InitiateReq) (*InitiateRsp, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Initiate not implemented")
}
func (*UnimplementedRpcServer) Abort(context.Context, *AbortReq) (*AbortRsp, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Abort not implemented")
}
func (*UnimplementedRpcServer) Complete(context.Context, *CompleteReq) (*CompleteRsp, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Complete not implemented")
}

func RegisterRpcServer(s *grpc.Server, srv RpcServer) {
	s.RegisterService(&_Rpc_serviceDesc, srv)
}

func _Rpc_Add_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(AddReq)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RpcServer).Add(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/nezha.upload.Rpc/Add",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RpcServer).Add(ctx, req.(*AddReq))
	}
	return interceptor(ctx, in, info, handler)
}

func _Rpc_Get_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetReq)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RpcServer).Get(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/nezha.upload.Rpc/Get",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RpcServer).Get(ctx, req.(*GetReq))
	}
	return interceptor(ctx, in, info, handler)
}

func _Rpc_Initiate_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(InitiateReq)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RpcServer).Initiate(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/nezha.upload.Rpc/Initiate",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RpcServer).Initiate(ctx, req.(*InitiateReq))
	}
	return interceptor(ctx, in, info, handler)
}

func _Rpc_Abort_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(AbortReq)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RpcServer).Abort(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/nezha.upload.Rpc/Abort",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RpcServer).Abort(ctx, req.(*AbortReq))
	}
	return interceptor(ctx, in, info, handler)
}

func _Rpc_Complete_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CompleteReq)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RpcServer).Complete(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/nezha.upload.Rpc/Complete",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RpcServer).Complete(ctx, req.(*CompleteReq))
	}
	return interceptor(ctx, in, info, handler)
}

var _Rpc_serviceDesc = grpc.ServiceDesc{
	ServiceName: "nezha.upload.Rpc",
	HandlerType: (*RpcServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "Add",
			Handler:    _Rpc_Add_Handler,
		},
		{
			MethodName: "Get",
			Handler:    _Rpc_Get_Handler,
		},
		{
			MethodName: "Initiate",
			Handler:    _Rpc_Initiate_Handler,
		},
		{
			MethodName: "Abort",
			Handler:    _Rpc_Abort_Handler,
		},
		{
			MethodName: "Complete",
			Handler:    _Rpc_Complete_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "nezha/upload/rpc.proto",
}
